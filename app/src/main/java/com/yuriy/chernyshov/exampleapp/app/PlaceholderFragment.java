package com.yuriy.chernyshov.exampleapp.app;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 5/8/14
 * Time: 8:23 PM
 */
public class PlaceholderFragment extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String LOG_TAG = PlaceholderFragment.class.getSimpleName();

    public static final int MENU_FRAGMENT = Menu.FIRST;
    public static final int MENU_REMOVE = Menu.FIRST + 1;

    private static final String PREFS_NAME = "PlaceholderFragmentPrefsFile";
    private static final String PREFS_CHECK_BOX = "PREFS_CHECK_BOX";

    private TextView mTextView;
    private CheckBox mCheckBox;
    private BaseFragment mBaseFragment;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
        Log.d(LOG_TAG, "Constructor, hashCode:" + hashCode());
        mBaseFragment = new BaseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "On Create:" + savedInstanceState + ", hashCode:" + hashCode());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mTextView = (TextView) rootView.findViewById(R.id.text_view);
        mCheckBox = (CheckBox) rootView.findViewById(R.id.static_check_box);
        Log.d(LOG_TAG, "On Create View:" + savedInstanceState + ", hashCode:" + hashCode());

        //FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        //mBaseFragment = (BaseFragment) fragmentManager.findFragmentByTag("BFT");
        //if (mBaseFragment == null) {
        //    mBaseFragment = new BaseFragment();
        //}

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.base_fragment, mBaseFragment, "BFT");
        transaction.commit();

        restoreViewState();

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(LOG_TAG, "On Pause, hashCode:" + hashCode());

        saveViewState();
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.d(LOG_TAG, "On Stop, hashCode:" + hashCode());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.d(LOG_TAG, "On Destroy View, hashCode:" + hashCode());
    }

    @Override
    public void onDetach() {
        super.onDetach();

        Log.d(LOG_TAG, "On Detach, hashCode:" + hashCode());
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(LOG_TAG, "On Resume, hashCode:" + hashCode());

        Button button = (Button) mBaseFragment.getView().findViewById(R.id.button_view);
        button.setText("Button " + getArguments().getInt(ARG_SECTION_NUMBER));

        mTextView.setText("Fragment " + getArguments().getInt(ARG_SECTION_NUMBER) + ", hash:" +
            hashCode());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //getActivity().getMenuInflater().inflate(R.menu.main, menu);
        //menu.add(0, MENU_FRAGMENT, Menu.NONE, "Menu " + getArguments().getInt(ARG_SECTION_NUMBER));
        //menu.add(0, MENU_REMOVE, Menu.NONE, "Remove Fragment");
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        Log.d(LOG_TAG, "TRACE: " + isVisible() + " " + isAdded() + " " + isHidden());
        if (!isVisible()) {
            return;
        }

        if (menu.findItem(MENU_FRAGMENT) == null) {
            menu.add(0, MENU_FRAGMENT, Menu.NONE, "Menu " + getArguments().getInt(ARG_SECTION_NUMBER));
        }
        if (menu.findItem(MENU_REMOVE) == null) {
            menu.add(0, MENU_REMOVE, Menu.NONE, "Remove Fragment");
        }

        Log.d(LOG_TAG, "PrepareOptionsMenu");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case MENU_REMOVE:
                ((MainActivity) getActivity()).removeFragment(getArguments()
                        .getInt(ARG_SECTION_NUMBER) - 1);
        }

        return super.onOptionsItemSelected(item);
    }

    private void restoreViewState() {
        // Restore preferences
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        boolean checkBoxChecked = settings.getBoolean(PREFS_CHECK_BOX, false);

        mCheckBox.setChecked(checkBoxChecked);
    }

    private void saveViewState() {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(PREFS_CHECK_BOX, mCheckBox.isChecked());

        // Commit the edits!
        editor.commit();
    }
}